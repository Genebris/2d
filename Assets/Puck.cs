﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using GameAnalyticsSDK;

public class Puck : MonoBehaviour
{
	Vector2 startPosition;
	Rigidbody2D rb;

	public Text LevelText;
	public int lastLevel;

    // Start is called before the first frame update
    void Start()
    {
		GameAnalytics.Initialize();
        rb = GetComponent<Rigidbody2D>();
		LevelText.text = SceneManager.GetActiveScene().name;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
			startPosition = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);


		if (Input.GetMouseButtonUp(0)) {
			Vector2 current = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
			Vector2 force = current - startPosition;
			rb.AddForce(force);
		}
    }


	void OnCollisionEnter2D(Collision2D	 other) {
		string tag = other.gameObject.tag;
		if (tag == "Obstacle")
			SceneManager.LoadScene(SceneManager.GetActiveScene().name);

		if (tag == "Gate") {
			int nextScene = int.Parse(SceneManager.GetActiveScene().name)+1;
			GameAnalytics.NewDesignEvent ("LevelCompleted", nextScene-1f);
			if (nextScene<=lastLevel)
				SceneManager.LoadScene(nextScene.ToString());
			else
				SceneManager.LoadScene(SceneManager.GetActiveScene().name);

		}

	}

}
